import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Todo} from "../model/todo"
@Injectable({
  providedIn: 'root'
})
export class DataService {

constructor(
private  http:HttpClient
) { }


getAll(){
return this.http.get("http://localhost:3000/toDo")
}

updateTodo(id:number,data:any){
  return this.http.put(`http://localhost:3000/toDo/${id}`,data)
}

saveTodo(data:Todo){
return this.http.post("http://localhost:3000/toDo/post",data)
}

deleteTodo(id:number){
  return this.http.delete(`http://localhost:3000/toDo/${id}`)
}
viewTodo(id:number){
  return this.http.get(`http://localhost:3000/toDo/${id}`)
}

compelteToDo(id:number,data:any){
  return this.http.put(`http://localhost:3000/toDo/complete/6155a610f6fb738a2e936066`,data)
}

}
