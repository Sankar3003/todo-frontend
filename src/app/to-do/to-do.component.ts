import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { from } from 'rxjs';
import { DataService } from '../Services/data.service';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit {
  todoList: any=[];
  @ViewChild('contactForm') contactForm:NgForm | undefined
f:any={
  title:"",
  isCompleted:false
}
isCompleted:boolean=false
  isUpdate: boolean=false;
  private _id: any;
  constructor(private _dataService:DataService) { }

  ngOnInit(): void {
    this.toDoAll()
  }


toDoAll(){
this._dataService.getAll().subscribe((res:any)=>{
  console.log(res);
  var data:[]=res

  this.todoList=data.filter((e:any)=>{
    console.log("E",e.isCompleted)
return e.isCompleted==false
  })
  console.log("todoList",this.todoList)
})
}

onSubmit(vvform:any){
  console.log("ff" , vvform.form.value);

  this._dataService.saveTodo(vvform.form.value).subscribe(res=>{
    if(res){
this.toDoAll();
this.isUpdate=false
this.f.title=""

    }
  })
}


viewNote(id:number){
this._dataService.viewTodo(id).subscribe((res:any)=>{
  this.f.title=res.title
  this.f.isCompleted=res.isCompleted;
  this._id= res._id
  this.isUpdate=true
})
}

delteNote(id:number){
  this._dataService.deleteTodo(id).subscribe((res:any)=>{
    this.f.title=res.title
    this.f.isCompleted=res.isCompleted
    this.toDoAll();
  })
}

updateOne(){
  this._dataService.updateTodo(this._id,this.f).subscribe((res:any)=>{
  if(res){
    this.toDoAll();
this.isUpdate=false
this.f.title=""
  }
  })
}
completeTodo(id:number,event:any){
console.log("check " , event)
var update= {
  "isCompleted":event
}
this._dataService.compelteToDo(id,update ).subscribe(res=>{

  console.log("res",res)
  if(res){
    this.toDoAll()
  }
})

}
}
